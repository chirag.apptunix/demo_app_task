import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { TaskManagerService } from "../services/task-manager.service";

@Component({
  selector: "ngx-settings",
  templateUrl: "./settings.component.html",
  styleUrls: ["./settings.component.scss"],
})
export class SettingsComponent implements OnInit {
  constructor(
    private TaskManager: TaskManagerService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  hideCompletedTasks() {
    for (let i = 0; i < this.TaskManager.tasksList.length; i++) {
      if (this.TaskManager.tasksList[i].status === true) {
        this.TaskManager.tasksList[i].show = false;
        this.router.navigate(["/task-list"]);
      }
    }
  }

  showCompletedTasks() {
    for (let i = 0; i < this.TaskManager.tasksList.length; i++) {
      if (this.TaskManager.tasksList[i].status === true) {
        this.TaskManager.tasksList[i].show = true;
        this.router.navigate(["/task-list"]);
      }
    }
  }

  hideUrgentTasks() {
    for (let i = 0; i < this.TaskManager.tasksList.length; i++) {
      if (this.TaskManager.tasksList[i].taskType === "Urgent") {
        console.log("URGENT", this.TaskManager.tasksList[i]);
        this.TaskManager.tasksList[i].show = false;
        this.router.navigate(["/task-list"]);
      }
    }
  }

  showUrgentTasks() {
    for (let i = 0; i < this.TaskManager.tasksList.length; i++) {
      if (this.TaskManager.tasksList[i].taskType === "Urgent") {
        this.TaskManager.tasksList[i].show = true;
        this.router.navigate(["/task-list"]);
      }
    }
  }

  hideNotUrgentTasks() {
    for (let i = 0; i < this.TaskManager.tasksList.length; i++) {
      if (this.TaskManager.tasksList[i].taskType === "!Urgent") {
        this.TaskManager.tasksList[i].show = false;
        this.router.navigate(["/task-list"]);
      }
    }
  }

  showNotUrgentTasks() {
    for (let i = 0; i < this.TaskManager.tasksList.length; i++) {
      if (this.TaskManager.tasksList[i].taskType === "!Urgent") {
        this.TaskManager.tasksList[i].show = true;
        this.router.navigate(["/task-list"]);
      }
    }
  }
}
