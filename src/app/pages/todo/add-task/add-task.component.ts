import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { TaskManagerService } from "../services/task-manager.service";

@Component({
  selector: "ngx-add-task",
  templateUrl: "./add-task.component.html",
  styleUrls: ["./add-task.component.scss"],
})
export class AddTaskComponent implements OnInit {
  task: string;
  taskType;
  showError: boolean = false;

  constructor(
    private TaskManager: TaskManagerService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  submit() {
    if (
      this.task !== "" &&
      this.task !== undefined &&
      this.task !== null &&
      this.taskType !== "" &&
      this.taskType !== undefined &&
      this.taskType !== null
    ) {
      let id = this.TaskManager.tasksList.length + 1;

      let newTask = {
        id,
        task: this.task,
        taskType: this.taskType,
        status: false,
        show: true,
      };
      this.TaskManager.tasksList.push(newTask);

      this.router.navigate(["/task-list"]);
    } else {
      this.showError = true;
    }
  }
}
