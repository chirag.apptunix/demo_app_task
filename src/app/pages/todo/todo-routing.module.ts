import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AddTaskComponent } from "./add-task/add-task.component";
import { EditTaskComponent } from "./edit-task/edit-task.component";
import { SettingsComponent } from "./settings/settings.component";
import { TaskListComponent } from "./task-list/task-list.component";

const routes: Routes = [
  {
    path: "",
    redirectTo: "task-list",
    pathMatch: "full",
  },
  {
    path: "task-list",
    component: TaskListComponent,
  },
  {
    path: "edit-task/:id",
    component: EditTaskComponent,
  },
  {
    path: "add-task",
    component: AddTaskComponent,
  },
  {
    path: "settings",
    component: SettingsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TodoRoutingModule {}
